#!/bin/bash

###########################################################################################################
# Script to scan a webserver host for the Logjam cipher-downgrade vulnerability ( CVE-2015-4000 )         #
# Author: John Chronister 	Date: 07DEC2016 	Script Name:  schauberger.sh			  #
# Name Significance:  Viktor Schauberger was good at getting rid of log jams 				  #
# NOTES:  I based this script on the materials found here: https://bettercrypto.org/ 			  #
###########################################################################################################


###########################################################################################################
#  How to fix issues found:	https://weakdh.org/sysadmin.html					  #
###########################################################################################################

DHP=`echo | openssl s_client -connect $1:443 -cipher "EDH" 2>/dev/null | grep -ie "Server .* key" | grep "DH\," | awk '{ print $5 }'`
KEY=`echo | openssl s_client -connect $1:443 -cipher "EDH" 2>/dev/null | grep -ie "Server .* key" | grep -v "DH\," | awk '{ print $5 }'`

openssl version | egrep '1\.0\.[2-9]|[1-9]\.[1-9]\.[0-9]' > /dev/null
if [ $? = 1 ]; then
     echo "OpenSSL version detected:"
     openssl version
     echo ""
     echo "OpenSSL 1.0.2 or newer required to output DH-Parameter information."
     exit 1
fi

if [ -z "$1" ]; then
echo ""
echo "Please provide hostname or IP address argument."
echo ""
exit 1
fi

if [ -z "$DHP" ] || [ -z "$KEY" ]; then
    echo "Unable to verify OpenSSL parameters for $1.  Sorry about that."
    exit 1
fi

if [[ ! "$DHP" > "1024" ]]; then
    echo "$1 has DH-Paramter of 1024 bits or less!	*** VULNERABLE ***"
    echo "$1 DH-Parameter: $DHP  Server Key: $KEY"
    exit 0
elif [[ "$KEY" > "$DHP" ]]; then
    echo "$1 has DH-Parameter smaller than the Server key!	*** WARNING ***"
    echo "$1 DH-Parameter: $DHP  Server Key: $KEY"
    exit 0
else 
    echo "$1 is OK	### OK ###"
    echo "$1 DH-Parameter: $DHP  Server Key: $KEY"
    exit 0
fi
